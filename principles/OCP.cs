﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solid_principles.principles
{
    class OCP : IRunnable
    {
        public string Name
        {
            get
            {
                return "Open/Closed Principle (OCP)";
            }
        }

        public void Run()
        {
            Shape[] shapes = [
                new Rectangle() { Width = 2, Height = 2 },
                new Circle() { Radius = 2 }
            ];

            foreach ( Shape shape in shapes ) {
                Console.WriteLine($"Area of {shape.GetType().Name} is {shape.CalculateArea()}");
            }
        }
    }

    // Before OCP
    //public class Rectangle
    //{
    //    public double Width { get; set; }
    //    public double Height { get; set; }
    //}

    //public class AreaCalculator
    //{
    //    public double CalculateArea(Rectangle rectangle)
    //    {
    //        return rectangle.Width * rectangle.Height;
    //    }
    //}

    // After OCP
    public abstract class Shape
    {
        public abstract double CalculateArea();
    }

    public class Rectangle : Shape
    {
        public double Width { get; set; }
        public double Height { get; set; }

        public override double CalculateArea()
        {
            return Width * Height;
        }
    }

    public class Circle : Shape
    {
        public double Radius { get; set; }

        public override double CalculateArea()
        {
            return Math.PI * Radius * Radius;
        }
    }

}
