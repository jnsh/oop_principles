﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solid_principles.principles
{
    class LSP : IRunnable
    {
        public string Name
        {
            get
            {
                return "Liskov Substitution Principle (LSP)";
            }
        }

        public void Run()
        {
            ICollection[] collections = [
                new Collection(),
                new PositiveNumbersCollection(),
            ];

            int[] items = [1, 5, -6, 10];
            
            foreach (ICollection collection in collections)
            {
                Console.WriteLine($"Running {collection.GetType().Name}");
                for (int i = 0; i < items.Length; i++)
                {
                    Console.WriteLine($"Adding {items[i]}");
                    try
                    {
                        collection.AddItem(items[i]);
                    } catch (Exception e) {
                        Console.WriteLine($"Error adding {items[i]}");
                    }
                }
            }
        }
    }

    // Violating LSP
    //public class Collection
    //{
    //    private List<int> items = new List<int>();

    //    public virtual void AddItem(int item)
    //    {
    //        items.Add(item);
    //    }

    //    public virtual int GetTotal()
    //    {
    //        return items.Sum();
    //    }
    //}

    //public class PositiveNumbersCollection : Collection
    //{
    //    public override void AddItem(int item)
    //    {
    //        if (item > 0)
    //        {
    //            base.AddItem(item);
    //        }
    //        else
    //        {
    //            throw new ArgumentException("Only positive numbers allowed.");
    //        }
    //    }
    //}

    // Adhering LSP
    public interface ICollection
    {
        void AddItem(int item);
        int GetTotal();
    }

    public class Collection : ICollection
    {
        private List<int> items = new List<int>();

        public virtual void AddItem(int item)
        {
            items.Add(item);
        }

        public virtual int GetTotal()
        {
            return items.Sum();
        }
    }

    public class PositiveNumbersCollection : ICollection
    {
        private List<int> positiveItems = new List<int>();

        public void AddItem(int item)
        {
            if (item > 0)
            {
                positiveItems.Add(item);
            }
            else
            {
                throw new ArgumentException("Only positive numbers allowed.");
            }
        }

        public int GetTotal()
        {
            return positiveItems.Sum();
        }
    }


}
